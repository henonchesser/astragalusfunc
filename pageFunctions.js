/*
    JS controller for the Astragalus roller demo page
    Expects: Jquery
    Authored: Henon Chesser
*/


//override form 
$( "#astraForm" ).submit(function( event ) {
    event.preventDefault();
    var rollPostArray = {};
    
    $(".diceSelect").each( function(){
        var thisDice = this.name.replace("dice", "");
        var thisValue = this.value;


        if ($.isNumeric(thisValue) && thisValue >=0 && thisValue <= 6) rollPostArray[thisDice] = thisValue;
        
    })

    console.log(rollPostArray);

    $.ajax({
        type: "POST",
        dataType: "JSON",
        data: {roll:rollPostArray},
        url: "rollerFunctions.php",
        success: function(returnData){
          console.log(returnData);
          replaceRollValues(returnData);
        },
        fail: function() {
            console.log("fail");
        }

     });

    


});

function replaceRollValues(rollArray){
    $.each( rollArray, function( key, value ) {
        $("#" + key + "dice").val(value);
      });
}

