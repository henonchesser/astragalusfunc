<?php include "rollerFunctions.php" ?>

<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

    <title>FA Test PAGE</title>
  </head>
  <body>
    

    <div class="container">
        <div class="row">
           <div class="col-sm">
                <h2>Control</h2>
           </div>
           <div class="col-sm-8">
                <h2>Results</h2>
           </div> 
        </div>
        <div class="row">
            <div class="col-sm">
                <form action="" id="astraForm">
                    <div class="form-row">
                        <div class="col">
                            <div class="form-group">
                                <label for="fdice">Fruit and Fire</label>
                                <select name="fdice" id="fdice" class="diceSelect form-control">
                                    <option value="">Don't Roll</option>
                                    <option value="0">Roll</option>
                                    <optgroup label="Predefined">
                                        <option value="1">1</option>
                                        <option value="2">2</option>
                                        <option value="3">3</option>
                                        <option value="4">4</option>
                                        <option value="5">5</option>
                                        <option value="6">6</option>
                                    </optgroup>
                                </select>
                            </div>
                        </div>
                        <div class="col">
                            <div class="form-group">
                                <label for="bdice">Blade and Bone</label>
                                <select name="bdice" id="bdice" class="diceSelect form-control">
                                    <option value="">Don't Roll</option>
                                    <option value="0">Roll</option>
                                    <optgroup label="Predefined">
                                        <option value="1">1</option>
                                        <option value="2">2</option>
                                        <option value="3">3</option>
                                        <option value="4">4</option>
                                        <option value="5">5</option>
                                        <option value="6">6</option>
                                    </optgroup>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="col">
                            <div class="form-group">
                                <label for="wdice">Wrote and Wild</label>
                                <select name="wdice" id="wdice" class="diceSelect form-control">
                                    <option value="">Don't Roll</option>
                                    <option value="0">Roll</option>
                                    <optgroup label="Predefined">
                                        <option value="1">1</option>
                                        <option value="2">2</option>
                                        <option value="3">3</option>
                                        <option value="4">4</option>
                                        <option value="5">5</option>
                                        <option value="6">6</option>
                                    </optgroup>
                                </select>
                            </div>
                        </div>
                        <div class="col">
                            <div class="form-group">
                                <label for="gdice">Gold and Ghosts</label>
                                <select name="gdice" id="gdice" class="diceSelect form-control">
                                    <option value="">Don't Roll</option>
                                    <option value="0">Roll</option>
                                    <optgroup label="Predefined">
                                        <option value="1">1</option>
                                        <option value="2">2</option>
                                        <option value="3">3</option>
                                        <option value="4">4</option>
                                        <option value="5">5</option>
                                        <option value="6">6</option>
                                    </optgroup>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="col">
                            <div class="form-group">
                                <label for="tdice">Storm and Song</label>
                                <select name="tdice" id="tdice" class="diceSelect form-control">
                                    <option value="">Don't Roll</option>
                                    <option value="0">Roll</option>
                                    <optgroup label="Predefined">
                                        <option value="1">1</option>
                                        <option value="2">2</option>
                                        <option value="3">3</option>
                                        <option value="4">4</option>
                                        <option value="5">5</option>
                                        <option value="6">6</option>
                                    </optgroup>
                                </select>
                            </div>
                        </div>
                        <div class="col">
                            <div class="form-group">
                                <label for="tdice">Time and Travel</label>
                                <select name="tdice" id="tdice" class="diceSelect form-control">
                                    <option value="">Don't Roll</option>
                                    <option value="0">Roll</option>
                                    <optgroup label="Predefined">
                                        <option value="1">1</option>
                                        <option value="2">2</option>
                                        <option value="3">3</option>
                                        <option value="4">4</option>
                                        <option value="5">5</option>
                                        <option value="6">6</option>
                                    </optgroup>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="form-row justify-content-md-center">
                        <div class="col col-lg-6">
                            <div class="form-group">
                                <label for="ddice">Death and Destiny</label>
                                <select name="ddice" id="ddice" class="diceSelect form-control">
                                    <option value="">Don't Roll</option>
                                    <option value="0">Roll</option>
                                    <optgroup label="Predefined">
                                        <option value="1">1</option>
                                        <option value="2">2</option>
                                        <option value="3">3</option>
                                        <option value="4">4</option>
                                        <option value="5">5</option>
                                        <option value="6">6</option>
                                    </optgroup>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="form-row justify-content-md-center">
                        <input type="submit" class="btn btn-primary" value="Roll">
                    </div>

                </form>
            </div>
            <div class="col-sm-8">
            </div>
        </div>
    </div>

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script
    src="https://code.jquery.com/jquery-3.4.1.min.js"
    integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo="
    crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
    <script src="pageFunctions.js"></script>
  </body>
</html>