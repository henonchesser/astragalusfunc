<?php
/*
Fors Astragalus Roller Functions 
Authored by Henon Chesser

    The functions here will take an array of labelled dice, with our without preselected results, and return a json object with result of the roll


*/

    if(isset($_POST['roll'])){
        rollAstragalus($_POST['roll']);
    } else {
        return "Not what I expected";
    }


    function rollAstragalus($rollArray){
        //rollArray is an assoc array of a dice face letter and it's number
        //pairs that come in with a number 1 - 6 are static
        // a zero is the signal to roll that dice and overwite the zero
        foreach ( $rollArray as $rollFace => $rollValue){
            if ( $rollValue == 0){
                $newRoll = rand(1,6);
                $rollArray[$rollFace] = $newRoll;
            } else {
                //js keeps passing numbers in as strings
                //fix that here
                $rollArray[$rollFace] = (int)$rollValue;
            }
        }

        echo json_encode($rollArray);
    }



?>